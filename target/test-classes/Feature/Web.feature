Feature: Add users

  Scenario Outline: Add users successfully
    Given User opens browser "<Browser>" and launches Web Address "http://www.way2automation.com/angularjs-protractor/webtables/"
    And User clicks Add User Button
    And User enters "<firstname>","<lastname>","<usernamr>","<password>"
    And User selects "<customer>",and enters "<role>","<email>","<cellphone>"
    When User click on save button
    Then record saved successfully

    Examples:
      |Browser|firstname|lastname|username|password|customer|role|email|cellphone|
      |CHROME|Fname1|Lname1|user1|Pass1|Company AAA|Admin|admin@mail.com|082555|
      |CHROME|Fname2|Lname2|user2|Pass1|Company BBB|Customer|customer@mail.com|083444|
