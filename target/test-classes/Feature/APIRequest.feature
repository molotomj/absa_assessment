Feature: Perform API requests to produce list of breeds, subbreeds and random subbreed images

  Scenario: Perform an API request to produce a list of all dog breeds and Using code, verify “retriever” breed is within the list.
    Given End point "https://dog.ceo/api/breeds/list/all"
    When a request is send to the server
    Then Response is received with status 200
    And list of all breeds is displayed

  Scenario: Perform an API request to produce a list of sub-breeds for “retriever”.
    Given End point "https://dog.ceo/api/breed/retriever/list"
    When a request is send to the server
    Then Response is received with status 200
    And "golden" subbreed list is displayed.

  Scenario: Perform an API request to produce a random image / link for the sub-breed “golden”
    Given End point "https://dog.ceo/api/breed/hound/images/random"
    When a request is send to the server
    Then Response is received with status 200
    And "hound" subbreed random image is displayed.

