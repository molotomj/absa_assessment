$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Ilab.feature");
formatter.feature({
  "line": 1,
  "name": "Ilab",
  "description": "",
  "id": "ilab",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to Job link",
  "description": "",
  "id": "ilab;navigate-to-job-link",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "User opens browser \"\u003cBrowser\u003e\" and launches Web Address \"https://www.ilabquality.com\"",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User selects hyperlink Careers",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "User Selects Country name \"\u003cCountry\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "User clicks first available post",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User clicks on Apply online button",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The Applicant name \"\u003cNames\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Applicant Email address \"\u003cEmail\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Phone number",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "User click on Send Application button",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "Error message displayed \"\u003cErrorMessage\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 15,
  "name": "",
  "description": "",
  "id": "ilab;navigate-to-job-link;",
  "rows": [
    {
      "cells": [
        "Browser",
        "Country",
        "Names",
        "Email",
        "ErrorMessage"
      ],
      "line": 16,
      "id": "ilab;navigate-to-job-link;;1"
    },
    {
      "cells": [
        "CHROME",
        "South Africa",
        "Bennett Malapane",
        "automationAssessment@iLABQuality.com",
        "You need to upload at least one file."
      ],
      "line": 17,
      "id": "ilab;navigate-to-job-link;;2"
    },
    {
      "cells": [
        "FIREFOX",
        "South Africa",
        "Bennett Malapane",
        "automationAssessment@iLABQuality.com",
        "You need to upload at least one file."
      ],
      "line": 18,
      "id": "ilab;navigate-to-job-link;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 17,
  "name": "Navigate to Job link",
  "description": "",
  "id": "ilab;navigate-to-job-link;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "User opens browser \"CHROME\" and launches Web Address \"https://www.ilabquality.com\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User selects hyperlink Careers",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "User Selects Country name \"South Africa\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "User clicks first available post",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User clicks on Apply online button",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The Applicant name \"Bennett Malapane\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Applicant Email address \"automationAssessment@iLABQuality.com\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Phone number",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "User click on Send Application button",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "Error message displayed \"You need to upload at least one file.\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CHROME",
      "offset": 20
    },
    {
      "val": "https://www.ilabquality.com",
      "offset": 54
    }
  ],
  "location": "Application.user_launches_Web_Address(String,String)"
});
formatter.result({
  "duration": 164164984275,
  "status": "passed"
});
formatter.match({
  "location": "Application.user_selects_hyperlink()"
});
formatter.result({
  "duration": 3739141124,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "South Africa",
      "offset": 27
    }
  ],
  "location": "Application.user_Selects_Country_name(String)"
});
formatter.result({
  "duration": 4371850503,
  "status": "passed"
});
formatter.match({
  "location": "Application.user_clicks_first_available_post()"
});
formatter.result({
  "duration": 2693138764,
  "status": "passed"
});
formatter.match({
  "location": "Application.user_clicks_on_Apply_online_button()"
});
formatter.result({
  "duration": 259498936,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bennett Malapane",
      "offset": 20
    }
  ],
  "location": "Application.the_Applicant_name(String)"
});
formatter.result({
  "duration": 516114706,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "automationAssessment@iLABQuality.com",
      "offset": 25
    }
  ],
  "location": "Application.applicant_Email_adress(String)"
});
formatter.result({
  "duration": 854455168,
  "status": "passed"
});
formatter.match({
  "location": "Application.phone_number()"
});
formatter.result({
  "duration": 283419416,
  "status": "passed"
});
formatter.match({
  "location": "Application.user_click_on_Send_Application_button()"
});
formatter.result({
  "duration": 3466146360,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "You need to upload at least one file.",
      "offset": 25
    }
  ],
  "location": "Application.error_message_displayed(String)"
});
formatter.result({
  "duration": 2255717229,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Navigate to Job link",
  "description": "",
  "id": "ilab;navigate-to-job-link;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "User opens browser \"FIREFOX\" and launches Web Address \"https://www.ilabquality.com\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User selects hyperlink Careers",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "User Selects Country name \"South Africa\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "User clicks first available post",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User clicks on Apply online button",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The Applicant name \"Bennett Malapane\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Applicant Email address \"automationAssessment@iLABQuality.com\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Phone number",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "User click on Send Application button",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "Error message displayed \"You need to upload at least one file.\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "FIREFOX",
      "offset": 20
    },
    {
      "val": "https://www.ilabquality.com",
      "offset": 55
    }
  ],
  "location": "Application.user_launches_Web_Address(String,String)"
});
formatter.result({
  "duration": 168018315102,
  "status": "passed"
});
formatter.match({
  "location": "Application.user_selects_hyperlink()"
});
formatter.result({
  "duration": 8860836,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "South Africa",
      "offset": 27
    }
  ],
  "location": "Application.user_Selects_Country_name(String)"
});
formatter.result({
  "duration": 28530281,
  "error_message": "org.openqa.selenium.NoSuchElementException: Unable to locate element: //a[contains(text(),\u0027South Africa\u0027)]\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027DESKTOP-TH1CU23\u0027, ip: \u0027192.168.56.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.firefox.FirefoxDriver\nCapabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 66.0.4, javascriptEnabled: true, moz:accessibilityChecks: false, moz:geckodriverVersion: 0.24.0, moz:headless: false, moz:processID: 18180, moz:profile: C:\\Users\\bmalapane\\AppData\\..., moz:shutdownTimeout: 60000, moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, platformVersion: 10.0, rotatable: false, setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify}\nSession ID: f9988428-5af6-48ca-a59e-b503bed363d7\n*** Element info: {Using\u003dxpath, value\u003d//a[contains(text(),\u0027South Africa\u0027)]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:428)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\r\n\tat TestSteps.Application.user_Selects_Country_name(Application.java:43)\r\n\tat ✽.And User Selects Country name \"South Africa\"(Ilab.feature:6)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "Application.user_clicks_first_available_post()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Application.user_clicks_on_Apply_online_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Bennett Malapane",
      "offset": 20
    }
  ],
  "location": "Application.the_Applicant_name(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "automationAssessment@iLABQuality.com",
      "offset": 25
    }
  ],
  "location": "Application.applicant_Email_adress(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Application.phone_number()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Application.user_click_on_Send_Application_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "You need to upload at least one file.",
      "offset": 25
    }
  ],
  "location": "Application.error_message_displayed(String)"
});
formatter.result({
  "status": "skipped"
});
});