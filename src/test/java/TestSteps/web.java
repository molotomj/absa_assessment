package TestSteps;

import CommonUtils.Constant;
import CommonUtils.WebObj;
import CommonUtils.launchUrl;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

public class web {
    @Given("^User opens browser \"([^\"]*)\" and launches Web Address \"([^\"]*)\"$")
    public void user_opens_browser_and_launches_Web_Address(String arg1, String arg2) throws Throwable {
        launchUrl.execute(arg1,arg2);
//        Constant.driver = driver;
        WebDriver driver = Constant.driver;
    }

    @And("^User clicks Add User Button$")
    public void user_clicks_Add_User_Button() throws Throwable {
        WebObj.click("adduser");

    }

    @And("^User enters \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
    public void user_enters(String arg1, String arg2, String arg3, String arg4) throws Throwable {
        WebObj.setText("firstname",arg1);
        WebObj.setText("lastname",arg2);
        WebObj.setText("username",arg3);
        WebObj.setText("password",arg4);

    }

    @And("^User selects \"([^\"]*)\",and enters \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
    public void user_selects_and_enters(String arg1, String arg2, String arg3, String arg4) throws Throwable {
      // WebObj.selecttext("");

    }

    @When("^User click on save button$")
    public void user_click_on_save_button() throws Throwable {

    }

    @Then("^record saved successfully$")
    public void record_saved_successfully() throws Throwable {
    }


}
