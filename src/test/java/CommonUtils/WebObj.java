package CommonUtils;

import java.util.HashMap;
import java.util.Map;


import Object_MAP.createuser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class WebObj
{
    public static WebDriver driver = Constant.driver;
	public static Map<String, String> page_Objects = new HashMap<String, String>();
	public static WebElement element = null;
	public static String browser; 
	public static Map<String, String> dictionary = new HashMap<String, String>();
		
	public static WebElement getElement(String str)
	{
		createuser.ObjMap_createuser();
		try
			{
				String desc=page_Objects.get(str);
				String [] a= desc.split("\\|");
				switch(a[0])
				{
					case "ID":  return driver.findElement(By.id(a[1]));
					case "CLASSNAME":  return driver.findElement(By.className(a[1]));
					case "LINKTEXT":  return driver.findElement(By.linkText(a[1]));
					case "NAME":  return driver.findElement(By.name(a[1]));
					case "xpath":  return driver.findElement(By.xpath(a[1]));
					//My_Page_Obejcts.put("Username", "xpath|//[contains(@id,'j_username')");
				//	driver.findElement(By.xpath("//input[contains(@id,'j_username')]"));
					
					default: System.out.println("Function getElement cannot return object for " + str);break;
				}
			}
		catch(Exception e)
			{
				System.out.println("Exeption in WebObj.getElement - ");
				return null;
			}
		return null;		
	}
	
	public static void click(String elementName) throws Exception
	{
		createuser.ObjMap_createuser();
		String strError = null;
		try
		{
			WebElement elmn=getElement( elementName);
			if ((elmn).isDisplayed() && elmn.isEnabled())
			{	
				

				System.out.println("Click element: "+elementName);
				elmn.click();
						if (driver.findElement(By.xpath("//span[contains(@id,'wealthForm:outputTextId')]")).isDisplayed());
				{
					

					strError = driver.findElement(By.xpath("//span[contains(@id,'wealthForm:dataTableId:0:col1_outputTextId')]")).getAttribute("innerHTML");
					System.out.println(strError);
									

				}
			//	HtmlReporter.WriteStep(description, "Click", "Clicked" , true );
			}
			else
			{
			//	HtmlReporter.WriteStep("Object not visible - " + description, "Click", "Not Clicked" , false );
			}
		}
		catch(Exception e)
		{System.out.println("Exeption in WebObj.click - "+elementName);}
	}
	
	public static void setText( String elementName, String textToSet) throws Exception
	{
		createuser.ObjMap_createuser();
		try
		{
			if(textToSet!=null)
			{
				WebElement elmn=getElement(elementName);
				if(elmn.isDisplayed())
				{
					elmn.clear();
					System.out.println("Populate Textbox: "+elementName+" With value: "+textToSet);
					elmn.sendKeys(textToSet);

				}
				else
				{

				}
			}
		}
		catch(Exception e)
			{
				System.out.println("Exeption in WebObj.setText - "+elementName);
			}
		
	}
	
	public static void selecttext( String elementName, String textToSet) throws Exception
	{
		createuser.ObjMap_createuser();
		try
		{
			if(textToSet!=null)
			{
				WebElement elmn=getElement(elementName);
				if(elmn.isDisplayed())
				{
					System.out.println("Populate drop down Field: "+elementName+" With value: "+textToSet);
					elmn.sendKeys(textToSet);

				}
				else
				{

				}
			}
		}
		catch(Exception e)
			{
				System.out.println("Exeption in WebObj.setText - "+elementName);
			}
			
	}
	
	public static String gettext( String elementName) throws Exception
	{
        createuser.ObjMap_createuser();
		String strGettxt = null;
		
		try
		{
			
			
				WebElement elmn=getElement(elementName);
				if(elmn.isDisplayed())
				{
				strGettxt = elmn.getText();

				}
				else
				{
					}
			
		}
		catch(Exception e)
			{
				System.out.println("Exeption in WebObj.setText - "+elementName);
			}
		return strGettxt;
	}
	
}
