package CommonUtils;

import org.apache.poi.ss.usermodel.Sheet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Utils {


    public static int intGetColumnIndex(Sheet MySheet, String Columntext)
    {
        int intCellIndex = 0;
        int  row = MySheet.getFirstRowNum();

        String ColumnName = Columntext;
        int colCount;
        colCount = MySheet.getRow(0).getLastCellNum();
        for (int colIndex = 0; colIndex < colCount; colIndex++)
        {
            String strCellValue = MySheet.getRow(row).getCell(colIndex).getStringCellValue();

            if (ColumnName.equalsIgnoreCase(strCellValue))
            {
                intCellIndex =colIndex ;
                break;
            }
        }
        return intCellIndex;

    }

    public static ArrayList<String> readXmlFile(String Filename, String applicantid) throws Exception
    {
        String applicant = null;
        String strBrowser = null;
        String url = null;
        String country = null;
        String names = null;
        String email = null;
        String cellno = null;
        String errormessage = null;

        try {

            File fXmlFile = new File(Filename+".xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("applicant");

            System.out.println("----------------------------");
            aa:
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    applicant = eElement.getAttribute("id");

                    System.out.println("applicant id : " + eElement.getAttribute("id"));
                    if (applicantid.equalsIgnoreCase(applicant))
                    {
                        strBrowser = eElement.getElementsByTagName("browser").item(0).getTextContent();
                         url =  eElement.getElementsByTagName("url").item(0).getTextContent();
                         country = eElement.getElementsByTagName("country").item(0).getTextContent();
                         names =  eElement.getElementsByTagName("names").item(0).getTextContent();
                         email =  eElement.getElementsByTagName("email").item(0).getTextContent();
                         cellno =  eElement.getElementsByTagName("cellno").item(0).getTextContent();
                         errormessage =  eElement.getElementsByTagName("errormessage").item(0).getTextContent();
                        //	System.out.println("Password : " + eElement.getElementsByTagName("Password").item(0).getTextContent());
                        break aa;
                    }

                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        ArrayList<String> list = new ArrayList<String>();
        list.add(strBrowser);
        list.add(url);
        list.add(country);
        list.add(names);
        list.add(email);
        list.add(cellno);
        list.add(errormessage);

        return list;
    }


    public static void SQLSelect()
    {
        try
        {
            // create our mysql database connection
            String myDriver = "com.ibm.db2.jcc.DB2Driver";
            Class.forName("com.ibm.db2.jcc.DB2Driver");
            String myUrl = "jdbc:db2://edb2dev5.metmom.mmih.biz:60000/lhqaapp";

            Connection conn = DriverManager.getConnection(myUrl, "lhdbuser", "pr0f1l3");

            // our SQL SELECT query.
            // if you only need a few columns, specify them by name instead of using "*"
            String query = "SELECT * FROM wealth.contract where Numeric = '502210101'";

            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next())
            {

                String Contract_Numeric = rs.getString("Numeric");
                String Status = rs.getString("currentstatusname");
                String Companylicense = rs.getString("companylicense_id");


                // print the results
                System.out.println( Contract_Numeric + Status + Companylicense);
            }
            st.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
}
